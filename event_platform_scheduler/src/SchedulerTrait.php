<?php

namespace Drupal\event_platform_scheduler;

/**
 * Provides reusable methods for the Event Platform Scheduler.
 */
trait SchedulerTrait {

  /**
   * Information about the entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Separator to join / divide workflow and state during storage.
   *
   * @var string
   */
  protected $separator = '||';

  /**
   * Helper function to return a nested array of workflows and related types.
   *
   * @param array $types_chosen
   *   The content types for which workflows should be found.
   *
   * @return array
   *   The nested array, or an empty array if no workflows apply.
   */
  public function getWorkflowsForTypes(array $types_chosen) {
    $workflows_processed = [];
    foreach ($types_chosen as $type) {
      $query = $this->entityTypeManager->getStorage('workflow')->getQuery();
      $query->condition('type_settings.entity_types.node.*', $type);
      $workflows = $query->execute();
      if ($workflows && is_array($workflows)) {
        // There should only be one workflow per bundle, so use the first one.
        $workflow = array_pop($workflows);
        $workflows_processed[$workflow][] = $type;
      }
    }
    return $workflows_processed;
  }

}
